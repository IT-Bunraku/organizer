from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta

from airflow.operators import *
from airflow.contrib.operators import *

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2016, 8, 1),
    'email': ['importer@tayaa.me'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
}

dag = DAG('import_perso', default_args=default_args, schedule_interval=timedelta(1))

##########################################################################################

class PatternSensor(BaseSensor):
    pass

#*****************************************************************************************

class PatternOperator(BaseOperator):
    pass

#*****************************************************************************************

def register_op(*args, **kwargs):
    def do_apply(handler, dag, task_id, *params, **context):
        upl = None

        if 'upstream' in context:
            upl = context['upstream']

            del context['upstream']

        resp = PythonOperator(prepare_ghost, op_args=params, op_kwargs=context, provide_context=True, dag=dag)

        if upl is not None:
            resp.set_upstream(upl)

        return resp

    return lambda hnd: do_apply(hnd, *args, **kwargs)

##########################################################################################

USER_GHOST = {}

USER_CREDs = {
    'mail': [
        dict(server=, pseudo='tayamino@gmail.com', passwd=''),
    ],
}

#*****************************************************************************************

@register_op(dag, 'setup_ghost', op_args=(USER_GHOST,))
def prepare_ghost(ghost):
    pass

#*****************************************************************************************

@register_op(dag, 'setup_cables', op_args=(USER_GHOST,))
def prepare_cables(ghost):
    pass

##########################################################################################

from pattern.web import Mail, GMAIL, SUBJECT

def parse_mail(ghost,entry):
    conn = Mail(username=entry['pseudo'], password=entry['passwd'], service=GMAIL)

    #print gmail.folders.keys() # ['drafts', 'spam', 'personal', 'work', 'inbox', 'mail', 'starred', 'trash']

#*****************************************************************************************

for entry in USER_CREDs['mail']:
    register_op(dag, 'parse_mail', upstream=setup_ghost, op_args=(USER_GHOST,))(parse_mail)

##########################################################################################

from pattern.web import Facebook, NEWS, COMMENTS, LIKES

@register_op(dag, 'cables_facebook', op_args=(USER_GHOST,))
def cables_facebook(ghost,entry):
    fb = Facebook(license=os.environ.get('API_FACEBOOK_TOKEN', ''))
    me = fb.profile(id=None)

    for post in fb.search(me['id'], type=NEWS, count=100):
        print repr(post.id)
        print repr(post.text)
        print repr(post.url)

        if post.comments > 0:
            print '%i comments' % post.comments
            print [(r.text, r.author) for r in fb.search(post.id, type=COMMENTS)]
        if post.likes > 0:
            print '%i likes' % post.likes
            print [r.author for r in fb.search(post.id, type=LIKES)]

# u'530415277_10151455896030278'
# u'Tom De Smedt likes CLiPS Research Center'
# u'http://www.facebook.com/CLiPS.UA'
# 1 likes
# [(u'485942414773810', u'CLiPS Research Center')]

#*****************************************************************************************

cfb = PythonOperator(cables_facebook, op_args=(ghost,), op_kwargs=None, provide_context=True, dag=dag)

cfb.set_upstream(tp)

##########################################################################################


#*****************************************************************************************

cwb = PythonOperator(cables_pocket, op_args=(ghost,), op_kwargs=None, provide_context=True, dag=dag)

cwb.set_upstream(tc)

##########################################################################################

t1 = BashOperator(
    task_id='ptn_mail',
    bash_command='date',
    dag=dag)

t2 = BashOperator(
    task_id='sleep',
    bash_command='sleep 5',
    retries=3,
    dag=dag)

t3 = BashOperator(
    task_id='templated',
    bash_command="""
    {% for i in range(5) %}
        echo "{{ ds }}"
        echo "{{ macros.ds_add(ds, 7)}}"
        echo "{{ params.my_param }}"
    {% endfor %}
""",
    params={
        'my_param': 'Parameter I passed in',
    },
    dag=dag)

##########################################################################################

t2.set_upstream(t1)
t3.set_upstream(t1)

##########################################################################################
##########################################################################################


##########################################################################################


##########################################################################################

from pattern.web import URL, DOM, plaintext

url = URL('http://www.reddit.com/top/')
dom = DOM(url.download(cached=True))
for e in dom('div.entry')[:3]: # Top 3 reddit entries.
    for a in e('a.title')[:1]: # First <a class="title">.
        print repr(plaintext(a.content))
 
# u'Invisible Kitty'
# u'Naturally, he said yes.'
# u"I'd just like to remind everyone that /r/minecraft exists and not everyone wants"
# u"to have 10 Minecraft posts a day on their front page."

##########################################################################################

from pattern.web import Crawler

class Polly(Crawler): 
    def visit(self, link, source=None):
        print 'visited:', repr(link.url), 'from:', link.referrer
    def fail(self, link):
        print 'failed:', repr(link.url)

p = Polly(links=['http://www.clips.ua.ac.be/'], delay=3)
while not p.done:
    p.crawl(method=DEPTH, cached=False, throttle=3)

# visited: u'http://www.clips.ua.ac.be/'
# visited: u'http://www.clips.ua.ac.be/#navigation'
# visited: u'http://www.clips.ua.ac.be/colloquia'
# visited: u'http://www.clips.ua.ac.be/computational-linguistics'
# visited: u'http://www.clips.ua.ac.be/contact'

##########################################################################################

from pattern.web import crawl

for link, source in crawl('http://www.clips.ua.ac.be/', delay=3, throttle=3):
    print link

# Link(url=u'http://www.clips.ua.ac.be/')
# Link(url=u'http://www.clips.ua.ac.be/#navigation')
# Link(url=u'http://www.clips.ua.ac.be/computational-linguistics') 

