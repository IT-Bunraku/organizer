    rules = (
    )
    
    def parse_offer(self, response):
        entry = response
        
        offer = JobOffer()
        
        uri = urlparse(response.url)
        
        offer['link']          = response.url
        offer['source']        = self.name
        offer['narrow']        = response.url.replace('offre/', '')
        
        if offer['narrow']:
            offer['title']         = cleanup(entry.xpath('.//div[@id="job-desc"]/h3/text()'), sep=None, trim=True)
            offer['summary']       = cleanup(entry.xpath('.//div[@id="job-info"]/text()'), sep=None, trim=True)
            offer['when']          = cleanup(entry.xpath('.//div[@id="view-right"]/span/text()'), sep=None, trim=True)
            
            offer['company'] = {
                'location': cleanup(entry.xpath('.//div[@id="view-left"]/a[@rel="tag"]/text()'), sep=None, trim=True),
            }
            
            offer['details']['title'] = cleanup(entry.xpath('.//div[@id="job-type"]/div/text()'), sep=None, trim=True)
            
            yield offer

