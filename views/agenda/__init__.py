#-*- coding: utf-8 -*-

from uchikoma.organizer.shortcuts import *

################################################################################

@Reactor.router.register_route('agenda', r'^$', strategy='login')
class Homepage(Reactor.ui.Dashboard):
    template_name = "contacts/views/homepage.html"

    def enrich(self, request, **context):
        context['primitives'] = enumerate_schemas(owner_id=request.user.pk)

        return context

    def populate(self, request, **context):
        for label,icon,stats in context['primitives']:
            yield Reactor.ui.Block('counter', label.lower(),
                title=label, icon=icon,
                size=dict(md=3, xs=6),
            value=stats)

################################################################################

# @Reactor.router.register_route('connect', r'^$', strategy='login')
@render_to('rest/identity/aggregate/calendar.html')
def calendrier(request, calendar='default'):
    return context(
        profile=request.user,
        calendar_id=calendar,
    )

#*******************************************************************************

def events(request, calendar='default'):
    mapping = {
        'default': request.user.calendar,
    }

    resp = []

    if calendar in mapping:
        for event in mapping[calendar]:
            entry = {
                'title': event.text,
                'url': event.permalink,
            }

            if event.start_at:
                entry['start'] = str(event.start_at)

                if getattr(event, 'ends_at', None) is not None:
                    entry['end'] = str(event.ends_at)
                else:
                    entry['allDay'] = True

            resp.append(entry)

    return JsonResponse(resp, safe=False)

