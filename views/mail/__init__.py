#-*- coding: utf-8 -*-

from uchikoma.organizer.shortcuts import *

from uchikoma.connector.schemas import *

################################################################################

class Homepage(Reactor.ui.Dashboard):
    template_name = "mail/views/homepage.html"

    def enrich(self, request, **context):
        context['primitives'] = enumerate_schemas(owner_id=request.user.pk)

        return context

    def populate(self, request, **context):
        for label,icon,stats in context['primitives']:
            yield Reactor.ui.Block('counter', label.lower(),
                title=label, icon=icon,
                size=dict(md=3, xs=6),
            value=stats)

@Reactor.router.register_route('mail', r'^$', strategy='login')
@render_to('rest/mail/list.html')
def homepage(request):
    qs = Mail_Message.objects(owner_id=request.user.pk)

    return context(
        profile=request.user,
        listing=qs,
    )

#*******************************************************************************

# @Reactor.router.register_route('connect', r'^$', strategy='login')
@render_to('rest/mail/list.html')
def folder_view(request):
    qs = Mail_Message.objects(owner_id=request.user.pk)

    return context(
        profile=request.user,
        listing=qs,
    )

#*******************************************************************************

@render_to('rest/mail/list.html')
def source_view(request, source):
    qs = Mail_Message.objects(owner_id=request.user.pk, source=source)

    return context(
        profile=request.user,
        listing=qs,
    )

################################################################################

# @Reactor.router.register_route('connect', r'^$', strategy='login')
@render_to('rest/mail/view.html')
def overview(request, source, narrow):
    qs = Mail_Message.objects(owner_id=request.user.pk, source=source, narrow=narrow)

    if len(qs):
        return context(
            profile=request.user,
            message=qs[0],
        )
    else:
        raise Http404("Mail message not found !")

