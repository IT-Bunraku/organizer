#-*- coding: utf-8 -*-

from uchikoma.organizer.shortcuts import *

################################################################################

@Reactor.router.register_route('contacts', r'^$', strategy='login')
class Homepage(Reactor.ui.Dashboard):
    template_name = "contacts/views/homepage.html"

    def enrich(self, request, **context):
        context['primitives'] = enumerate_schemas(owner_id=request.user.pk)

        return context

    def populate(self, request, **context):
        for label,icon,stats in context['primitives']:
            yield Reactor.ui.Block('counter', label.lower(),
                title=label, icon=icon,
                size=dict(md=3, xs=6),
            value=stats)

#*******************************************************************************

@render_to('rest/identity/aggregate/contacts.html')
def address_book(request):
    return context(
        profile=request.user,
        listing=request.user.contacts,
    )

################################################################################

@login_required
@render_to('rest/identity/graph.html')
def explorer(request, facet='people'):
    return context(partition=facet)

#*******************************************************************************

def graphiste(request, facet):
    mapping = {
        'people': dict(
            query=dict(
                source=['Person'],
                target=['Brand','Company'],
            ),
            custom=dict(
                Person=(lambda node,prop: prop['full']),
                Brand=(lambda node,prop: prop['title']),
            ),
            label=['name','full'],
        ),
        'networks': dict(
            query=dict(
                source=['OS','Appliance'],
                target=['Application','EmbeddedSystem','Platform'],
            ),
            custom=dict(
                OS=(lambda node,prop: prop['name']),
                Appliance=(lambda node,prop: prop['name']),
            ),
            label=['name','full'],
        ),
        'skills': dict(
            query=dict(
                source=['Person', 'Language','Framework'],
                target=['Platform','Application'],
            ),
            custom=dict(
                Person=(lambda node,prop: prop['full']),
                OS=(lambda node,prop: prop['name']),
                Appliance=(lambda node,prop: prop['name']),
            ),
            label=['name','full'],
        ),
    }

    narrow = facet

    if narrow in mapping:
        lookup = "MATCH s-[r]-t WHERE ("
        lookup += "s.ontology in ['%s']" % "','".join(mapping[narrow]['query']['source'])
        lookup += ") AND ("
        lookup += "t.ontology in ['%s']" % "','".join(mapping[narrow]['query']['target'])
        lookup += ") RETURN s,r"

        nodes,edges = view_cypher(VORTEX_DEVOPS, lookup, mapping[narrow]['label'], mapping[narrow]['custom'])

        return JsonResponse({
            'nodes': nodes,
            'edges': edges,
        })
    else:
        raise Http404()

