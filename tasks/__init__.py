from uchikoma.organizer.utils import *

from uchikoma.organizer.models  import *
from uchikoma.organizer.schemas import *
from uchikoma.organizer.graph   import *

################################################################################

@Reactor.rq.register_task(queue='perso')
def refresh_mailbox(mb_id, **response):
    box = MailBox.objects.get(pk=mb_id)

    lst = None

    for alias,folder in box.proxy.folders.iteritems():
        try:
            lst = [uid for uid in folder.search('')]
        except Exception,ex:
            lst = []

        refresh_mail_folder.delay(mb_id, alias, *lst)

#*******************************************************************************

@Reactor.rq.register_task(queue='perso')
def refresh_mail_folder(mb_id, folder, *targets):
    box = MailBox.objects.get(pk=mb_id)

    if folder in box.proxy.folders:
        directory = box.proxy.folders[folder]

        for uid in targets:
            msg = directory.read(uid)

            upsert(Mail_Message, *Mail_Message.from_mail(box.owner.pk, msg))

    print "*) Processed Mail message '%(username)s' at : %(hostname)s" % box.__dict__

################################################################################

@Reactor.rq.register_task(queue='perso')
def refresh_rssfeed(rss_id, **response):
    rss = RssFeed.objects.get(pk=rss_id)

    if rss.proxy:
        for entry in rss.proxy.entries:
            upsert(StoryPost, *StoryPost.from_feedparser(rss.owner.pk, entry))

        rss.title      = rss.proxy.feed.title
        rss.summary    = rss.proxy.feed.description

        if hasattr(rss.proxy.feed, 'updated'):
            rss.publish_at = dt_parser(rss.proxy.feed.updated)
        else:
            rss.publish_at = timezone.now()

        rss.save()

        print "*) Processed RSS Feed : %(link)s" % rss.__dict__
    else:
        print "*) Skipped RSS Feed : %(link)s" % rss.__dict__

################################################################################

@Reactor.rq.register_task(queue='perso')
def refresh_wordpress(wp_id, **response):
    site = WordpressSite.objects.get(pk=wp_id)

    for entry in site.get_posts():
        upsert(ArticlePost, *ArticlePost.from_wordpress(site.pk, entry))

        rss.title      = rss.proxy.feed.title
        rss.summary    = rss.proxy.feed.description

        site.save()

        print "*) Processed WP site : %(link)s" % rss.__dict__
    else:
        print "*) Skipped WP site : %(link)s" % rss.__dict__

