from django.conf.urls import include, url

from django.conf.urls.static import static

from gestalt.web.helpers import Reactor

from uchikoma.connector.views import Prime
from uchikoma.organizer.views import contacts as Views

################################################################################

urlpatterns = Reactor.router.urlpatterns('contacts',
    #url(r'^$',                              Views.Homepage.as_view(),        name='homepage'),
    url(r'^-(?P<facet>[^/]+)/$',            Views.explorer,        name='explorer'),
    url(r'^-(?P<facet>[^/]+)/graph.json$',  Views.graphiste,       name='graph.json'),

    url(r'^identities$',                    Prime.identity__list, name='identity__list'),
    url(r'^@(?P<owner>[^/]+)/$',            Prime.identity__view, name='identity__view'),

    url(r'^organizations$',                 Prime.org__list,      name='org__list'),
    url(r'^~(?P<owner>[^/]+)/$',            Prime.org__view,      name='org__view'),
)

