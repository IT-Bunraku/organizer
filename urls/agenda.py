from django.conf.urls import include, url

from django.conf.urls.static import static

from gestalt.web.helpers import Reactor

from uchikoma.organizer.views import agenda as Views

################################################################################

urlpatterns = Reactor.router.urlpatterns('agenda',
    #url(r'^$',                               Views.Homepage.as_view(), name='homepage'),
    url(r'^-(?P<calendar>.+)/$',             Views.calendrier,         name='calendar'),
    url(r'^-(?P<calendar>.+)/events\.json$', Views.events,             name='events'),
)

