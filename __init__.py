subdomains = {
    'mail':     dict(icon='envelope', title="MailBox"),
    'contacts': dict(icon='users',    title="contacts"),
    'agenda':   dict(icon='calendar', title="Agenda"),

    'cables': dict(icon='rss',      title="Cables"),
    'news':   dict(icon='rss',      title="Newswire"),
}

